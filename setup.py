from logging import DEBUG, getLogger, StreamHandler
from setuptools import setup
from subprocess import call
import sys


class BuildError(Exception):
    """Build failed."""
    pass


class Builder(object):

    _main_script = 'main.py'
    _bundle_name = 'Test'
    _bundle_identifier = 'test'
    _signing_identity = 'Self Signed'

    def __init__(self):
        super(Builder, self).__init__()
        self._logger = getLogger()

    def run(self):
        self._build()
        self._sign()

    def _build(self):
        """Build the application."""
        self._logger.debug('Building application')
        plist = {
            'CFBundleIdentifier': self._bundle_identifier,
            'CFBundleInfoDictionary': '6.0',
            'CFBundleName': self._bundle_name,
            'CFBundlePackageType': 'APPL',
            'CFBundleSignature': 'KPEM',
            'CFBundleVersion': '1.0',
        }
        options = {
            'argv_emulation': False,
            'plist': plist,
        }

        setup(
            app=[self._main_script],
            options={'py2app': options},
            setup_requires=['py2app'],
        )

    def _sign(self):
        """Signs the application."""
        self._logger.debug('Signing application')
        codesign_command = (
            'codesign',
            '-s',
            '{}'.format(self._signing_identity),
            '-vvvv',
            '-r=designated => identifier {}'.format(self._bundle_identifier),
            'dist/{}.app'.format(self._bundle_name),
        )
        self._logger.debug(' '.join(codesign_command))
        codesign_result = call(
            codesign_command,
            stdout=sys.stdout,
            stderr=sys.stderr,
        )
        self._logger.debug('Sign result: %s', codesign_result)


if __name__ == '__main__':
    stream_handler = StreamHandler()
    root_logger = getLogger()
    root_logger.addHandler(stream_handler)
    root_logger.setLevel(DEBUG)
    Builder().run()
