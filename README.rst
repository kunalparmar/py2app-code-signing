Setup
-----

Checkout the repo and install the required packages using -

.. code-block:: bash

    $ pip install -r requirements.txt

Build
-----

.. code-block:: bash

    $ python setup.py py2app

The application is signed using the certificate named "Self Signed".
If your certificate is named differently, change the variable named
`_signing_identity` in the `setup.py` script.
